#include "Mesh.h"
#include "MVP.h"

#include <glm/gtc/type_ptr.hpp>

#include <iostream>

Mesh::Mesh()
{
}

Mesh::~Mesh()
{
}

void Mesh::init(Vertex vertices[], int numVertices, GLushort indices[], int numIndices, GLSLShader* shader)
{
	//setup mesh vao and vbo stuff
	glGenVertexArrays(1, &m_pVaoID);
	glGenBuffers(1, &m_pVboID);
	glGenBuffers(1, &m_pVioID);
	GLsizei stride = sizeof(Vertex);

	//bind vao and vbo stuff
	glBindVertexArray(m_pVaoID);
	
		glBindBuffer(GL_ARRAY_BUFFER, m_pVboID);

		//pass mesh vertices to buffer objects
		glBufferData(GL_ARRAY_BUFFER, stride*numVertices, &vertices[0], GL_STATIC_DRAW);
	
		//enable vertex attribute array for position
		glEnableVertexAttribArray((*shader)["vVertex"]);
		glVertexAttribPointer((*shader)["vVertex"], 3, GL_FLOAT, GL_FALSE, stride, 0);
		
		//enable vertex attribute for color
		glEnableVertexAttribArray((*shader)["vColor"]);
		glVertexAttribPointer((*shader)["vColor"], 4, GL_FLOAT, GL_FALSE, stride, (const GLvoid*)offsetof(Vertex, color));
		
		//pass indices to element array buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pVioID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(GLushort), &indices[0], GL_STATIC_DRAW);

	//glBindVertexArray(0);
	
	std::cout << "Mesh init successful.\n";
}

void Mesh::update(float* rotation)
{
	(*rotation)+=1;
}

void Mesh::render(GLenum polygonMode, GLenum renderMode, int numIndices, GLSLShader* shader, float* rotation)
{
	shader->use();
	{
		glBindVertexArray(m_pVaoID);
		glUniformMatrix4fv((*shader)("MVP"), 1, GL_FALSE, glm::value_ptr(MVP::Instance()->getPMV()));
		glUniform1f((*shader)("theta"), *rotation);
		{
			//set polygon mode
			glPolygonMode(GL_FRONT_AND_BACK, polygonMode);
			//draw mesh
			glDrawElements(renderMode, numIndices, GL_UNSIGNED_SHORT, 0);
		}
	}
	shader->unUse();
}

void Mesh::close()
{
	//Destroy vao and vbo
	glDeleteBuffers(1, &m_pVboID);
	glDeleteBuffers(1, &m_pVioID);
	glDeleteVertexArrays(1, &m_pVaoID);

	cout << "mesh close successful\n";
}