#ifndef __Cube__
#define __Cube__

#include "Mesh.h"
#include "GLSLShader.h"

class Cube
{
public:
	Cube();
	~Cube();

	void init(GLSLShader* shader);
	void update();
	void render(GLSLShader* shader);
	void close();

private:

	Mesh m_pCubeMesh;
	Vertex m_pVertices[8];
	int m_pNumVertices;
	GLushort m_pIndices[36];
	int m_pNumIndices;
	GLenum m_pRendererMode;
	GLenum m_pPolygonMode;

	float m_pCubeRotation;
};

#endif
