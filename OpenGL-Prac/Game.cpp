#include "Game.h"
#include "Window.h"
#include "Triangle.h"

#include <SDL/SDL.h>

#include <iostream>

Game* Game::s_pInstance = 0;

Game::Game()
{
	m_pShader = new GLSLShader();
	m_pKey = new SDL_KeyboardEvent();
}

Game::~Game()
{
}

bool Game::init()
{
	Window::Instance()->createWindow("Physics Engine", 1024, 768, 0);

	//load shader
	m_pShader->loadFromFile(GL_VERTEX_SHADER, "shaders/vertShader.vert");
	m_pShader->loadFromFile(GL_FRAGMENT_SHADER, "shaders/fragShader.frag");

	//compile and link shader
	m_pShader->createAndLinkProgram();
	m_pShader->use();
	{
		//add attributes and uniforms
		m_pShader->addAttribute("vVertex");
		m_pShader->addAttribute("vColor");
		m_pShader->addUniform("MVP");
	}
	m_pShader->unUse();


	//m_pSphereGameObject.init();
	//m_pSquareGameObject.init();
	//m_pCircleGameObject.init();

	m_bRunning = true;
	return true;
}

void Game::handleEvents()
{
	SDL_Event event;

	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_QUIT:
				Game::Instance()->quit();
				break;

			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
					case SDLK_q:
						std::cout << "q pressed\n";
						m_pTriangleGameObject.init(m_pShader);
						m_pKey->keysym.sym = SDLK_q;
						break;

					case SDLK_w:
						std::cout << "w pressed\n";
						m_pCubeGameObject.init(m_pShader);
						m_pKey->keysym.sym = SDLK_w;
						break;

					case SDLK_e:
						std::cout << "e pressed\n";
						m_pSquareGameObject.init(m_pShader);
						m_pKey->keysym.sym = SDLK_e;
						break;

					case SDLK_r:
						std::cout << "r pressed\n";
						m_pCircleGameObject.init(m_pShader);
						m_pKey->keysym.sym = SDLK_r;
						break;

					case SDLK_t:
						std::cout << "t pressed\n";
						m_pSphereGameObject.init(m_pShader);
						m_pKey->keysym.sym = SDLK_t;
						break;
				}
		}
	}
}

void Game::update()
{
	m_pTriangleGameObject.update();
	m_pCubeGameObject.update();
	m_pSquareGameObject.update();
	m_pCircleGameObject.update();
	m_pSphereGameObject.update();
}

void Game::render()
{
	glClearDepth(1.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	switch (m_pKey->keysym.sym)
	{
		case SDLK_q:
			m_pTriangleGameObject.render(m_pShader);
			break;

		case SDLK_w:
			m_pCubeGameObject.render(m_pShader);
			break;

		case SDLK_e:
			m_pSquareGameObject.render(m_pShader);
			break;

		case SDLK_r:
			m_pCircleGameObject.render(m_pShader);
			break;

		case SDLK_t:
			m_pSphereGameObject.render(m_pShader);
			break;

		default:
			break;
	}
	//m_pCubeGameObject.render();
	//m_pSphereGameObject.render();
	//m_pSquareGameObject.render();
	//m_pCircleGameObject.render();

	Window::Instance()->swapBuffers();
}

void Game::close()
{
	m_pTriangleGameObject.close();
	m_pCubeGameObject.close();
	m_pSphereGameObject.close();
	m_pSquareGameObject.close();
	m_pCircleGameObject.close();

	//Destroy shader
	m_pShader->deleteShaderProram();

	SDL_DestroyWindow(Window::Instance()->getwindow());
}

void Game::quit()
{
	m_bRunning = false;
	SDL_Quit();
}
