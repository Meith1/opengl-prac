#ifndef __Triangle__
#define __Triangle__

#include "GLSLShader.h"
#include "Mesh.h"

class Triangle
{
public:

	Triangle();
	~Triangle();

	void init(GLSLShader* shader);
	void update();
	void render(GLSLShader* shader);
	void close();

private:

	Mesh m_pTriangleMesh;
	Vertex m_pVertices[3];
	int m_pNumVertices;
	GLushort m_pIndices[3];
	int m_pNumIndices;
	GLenum m_pRendererMode;
	GLenum m_pPolygonMode;
	
	float m_pTriangleRotation;
};

#endif
