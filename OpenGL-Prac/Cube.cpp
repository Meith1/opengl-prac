#include "Cube.h"

Cube::Cube()
{
}

Cube::~Cube()
{
}

void Cube::init(GLSLShader* shader)
{
	m_pNumVertices = 8;
	m_pNumIndices = 36;
	m_pPolygonMode = GL_FILL;
	m_pRendererMode = GL_TRIANGLES;

	for (int i = 0; i < 4; i++)
	{
		m_pVertices[i].color.r = 255;
		m_pVertices[i].color.b = 0;
		m_pVertices[i].color.g = 0;
		m_pVertices[i].color.a = 255;
	}

	for (int i = 4; i < 8; i++)
	{
		m_pVertices[i].color.r = 0;
		m_pVertices[i].color.b = 255;
		m_pVertices[i].color.g = 0;
		m_pVertices[i].color.a = 255;
	}

	//front face
	m_pVertices[0].position.x = -0.5f;
	m_pVertices[0].position.y = -0.5f;
	m_pVertices[0].position.z = +0.5f;

	m_pVertices[1].position.x = +0.5f;
	m_pVertices[1].position.y = -0.5f;
	m_pVertices[1].position.z = +0.5f;

	m_pVertices[2].position.x = +0.5f;
	m_pVertices[2].position.y = +0.5f;
	m_pVertices[2].position.z = +0.5f;

	m_pVertices[3].position.x = -0.5f;
	m_pVertices[3].position.y = +0.5f;
	m_pVertices[3].position.z = + 0.5f;

	//back face
	m_pVertices[4].position.x = -0.5f;
	m_pVertices[4].position.y = -0.5f;
	m_pVertices[4].position.z = -0.5f;

	m_pVertices[5].position.x = +0.5f;
	m_pVertices[5].position.y = -0.5f;
	m_pVertices[5].position.z = -0.5f;

	m_pVertices[6].position.x = +0.5f;
	m_pVertices[6].position.y = +0.5f;
	m_pVertices[6].position.z = -0.5f;

	m_pVertices[7].position.x = -0.5f;
	m_pVertices[7].position.y = +0.5f;
	m_pVertices[7].position.z = -0.5f;

	//front face
	m_pIndices[0] = 0;
	m_pIndices[1] = 1;
	m_pIndices[2] = 2;
	m_pIndices[3] = 0;
	m_pIndices[4] = 2;
	m_pIndices[5] = 3;

	//back face
	m_pIndices[6] = 4;
	m_pIndices[7] = 5;
	m_pIndices[8] = 6;
	m_pIndices[9] = 4;
	m_pIndices[10] = 6;
	m_pIndices[11] = 7;

	//left face
	m_pIndices[12] = 4;
	m_pIndices[13] = 0;
	m_pIndices[14] = 3;
	m_pIndices[15] = 4;
	m_pIndices[16] = 3;
	m_pIndices[17] = 7;

	//right face
	m_pIndices[18] = 1;
	m_pIndices[19] = 5;
	m_pIndices[20] = 6;
	m_pIndices[21] = 1;
	m_pIndices[22] = 6;
	m_pIndices[23] = 2;

	//top face
	m_pIndices[24] = 3;
	m_pIndices[25] = 2;
	m_pIndices[26] = 6;
	m_pIndices[27] = 3;
	m_pIndices[28] = 6;
	m_pIndices[29] = 7;

	//bottom face
	m_pIndices[30] = 0;
	m_pIndices[31] = 4;
	m_pIndices[32] = 5;
	m_pIndices[33] = 0;
	m_pIndices[34] = 5;
	m_pIndices[35] = 1;

	m_pCubeRotation = 0.f;

	m_pCubeMesh.init(m_pVertices, m_pNumVertices, m_pIndices, m_pNumIndices, shader);
}

void Cube::update()
{
	m_pCubeMesh.update(&m_pCubeRotation);
}

void Cube::render(GLSLShader* shader)
{
	m_pCubeMesh.render(m_pPolygonMode, m_pRendererMode, m_pNumIndices, shader, &m_pCubeRotation);
}

void Cube::close()
{
	m_pCubeMesh.close();
}