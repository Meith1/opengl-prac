#include "Triangle.h"
#include "MVP.h"

#include <glm/gtc/type_ptr.hpp>

#include <iostream>

using namespace std;

Triangle::Triangle()
{
}

Triangle::~Triangle()
{
}

void Triangle::init(GLSLShader* shader)
{
	//initialise all variables
	m_pNumVertices = 3;
	m_pNumIndices = 3;
	m_pPolygonMode = GL_FILL;
	m_pRendererMode = GL_TRIANGLES;

	//setup triangle vertices and geometry
	m_pVertices[0].color.r = 255;
	m_pVertices[0].color.g = 0;
	m_pVertices[0].color.b = 0;
	m_pVertices[0].color.a = 255;

	m_pVertices[1].color.r = 255;
	m_pVertices[1].color.g = 0;
	m_pVertices[1].color.b = 0;
	m_pVertices[1].color.a = 255;

	m_pVertices[2].color.r = 255;
	m_pVertices[2].color.g = 0;
	m_pVertices[2].color.b = 0;
	m_pVertices[2].color.a = 255;

	m_pVertices[0].position.x = -0.5;
	m_pVertices[0].position.y = -0.5; 
	m_pVertices[0].position.z = 0; 

	m_pVertices[1].position.x = 0;
	m_pVertices[1].position.y = 0.5;
	m_pVertices[1].position.z = 0;

	m_pVertices[2].position.x = 0.5;
	m_pVertices[2].position.y = -0.5;
	m_pVertices[2].position.z = 0;

	//setup triangle indices
	m_pIndices[0] = 0;
	m_pIndices[1] = 1;
	m_pIndices[2] = 2;

	m_pTriangleRotation = 0.f;

	std::cout << "Triangle init successful.\n";

	//call mesh init
	m_pTriangleMesh.init(m_pVertices, m_pNumVertices, m_pIndices, m_pNumIndices, shader);
}

void Triangle::update()
{
	m_pTriangleMesh.update(&m_pTriangleRotation);
}

void Triangle::render(GLSLShader* shader)
{
	m_pTriangleMesh.render(m_pPolygonMode, m_pRendererMode, m_pNumIndices, shader, &m_pTriangleRotation);
}

void Triangle::close()
{
	m_pTriangleMesh.close();
}
