#include "MVP.h"

MVP* MVP::s_MVP = 0;

MVP::MVP()
{
	 _P = glm::mat4(1);
	_MV = glm::mat4(1);
}

MVP::~MVP()
{
}

glm::mat4 MVP::getPMV()
{
	return _P*_MV;
}

void MVP::setP(glm::mat4 &p)
{
	_P = p;
}