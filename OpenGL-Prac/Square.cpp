#include "Square.h"

Square::Square()
{
}

Square::~Square()
{
}

void Square::init(GLSLShader* shader)
{
	//initialise all variables
	m_pNumVertices = 4;
	m_pNumIndices = 6;
	m_pPolygonMode = GL_FILL;
	m_pRendererMode = GL_TRIANGLES;

	//setup square vertices and geometry
	m_pVertices[0].color.r = 255;
	m_pVertices[0].color.g = 0;
	m_pVertices[0].color.b = 0;
	m_pVertices[0].color.a = 255;

	m_pVertices[1].color.r = 255;
	m_pVertices[1].color.g = 0;
	m_pVertices[1].color.b = 0;
	m_pVertices[1].color.a = 255;

	m_pVertices[2].color.r = 255;
	m_pVertices[2].color.g = 0;
	m_pVertices[2].color.b = 0;
	m_pVertices[2].color.a = 255;

	m_pVertices[3].color.r = 255;
	m_pVertices[3].color.g = 0;
	m_pVertices[3].color.b = 0;
	m_pVertices[3].color.a = 255;

	m_pVertices[0].position.x = -0.5;
	m_pVertices[0].position.y = -0.5;
	m_pVertices[0].position.z = 0;

	m_pVertices[1].position.x = +0.5;
	m_pVertices[1].position.y = -0.5;
	m_pVertices[1].position.z = 0;

	m_pVertices[2].position.x = +0.5;
	m_pVertices[2].position.y = +0.5;
	m_pVertices[2].position.z = 0;

	m_pVertices[3].position.x = -0.5;
	m_pVertices[3].position.y = +0.5;
	m_pVertices[3].position.z = 0;

	//setup square indices
	m_pIndices[0] = 0;
	m_pIndices[1] = 1;
	m_pIndices[2] = 2;
	m_pIndices[3] = 0;
	m_pIndices[4] = 2;
	m_pIndices[5] = 3;

	m_pSquareRoatation = 0;

	//call mesh init
	m_pSquareMesh.init(m_pVertices, m_pNumVertices, m_pIndices, m_pNumIndices, shader);
}

void Square::update()
{
	m_pSquareMesh.update(&m_pSquareRoatation);
}

void Square::render(GLSLShader* shader)
{
	m_pSquareMesh.render(m_pPolygonMode, m_pRendererMode, m_pNumIndices, shader, &m_pSquareRoatation);
}

void Square::close()
{
	m_pSquareMesh.close();
}
