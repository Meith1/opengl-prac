#ifndef __MVP__
#define __MVP__

#include <glm/glm.hpp>

class MVP
{
public:

	static MVP* Instance()
	{
		if (s_MVP == 0)
		{
			s_MVP = new MVP();
		}

		return s_MVP;
	}

	glm::mat4 getPMV();
	void setP(glm::mat4 &p);

private:
	MVP();
	~MVP();

	static MVP* s_MVP;

	glm::mat4  _P;
	glm::mat4 _MV;
};

#endif
