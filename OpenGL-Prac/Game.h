#ifndef __Game__
#define __Game__

#include "GLSLShader.h"
#include "Triangle.h"
#include "Cube.h"
#include "Sphere.h"
#include "Square.h"
#include "Circle.h"

#include <SDL/SDL.h>

class Game
{
public:
	
	static Game* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new Game();
			return s_pInstance;
		}

		return s_pInstance;
	}

	bool init();
	void handleEvents();
	void update();
	void render();
	void close();
	void quit();

	bool running() { return  m_bRunning; }

private:
	Game();
	~Game();

	static Game* s_pInstance;
	bool m_bRunning;

	GLSLShader* m_pShader;

	Triangle m_pTriangleGameObject;
	Cube m_pCubeGameObject;
	Sphere m_pSphereGameObject;
	Square m_pSquareGameObject;
	Circle m_pCircleGameObject;

	SDL_KeyboardEvent* m_pKey;
};

#endif
