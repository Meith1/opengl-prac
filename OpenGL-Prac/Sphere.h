#ifndef __Sphere__
#define __Sphere__

#include "Mesh.h"
#include "GLSLShader.h"

const int STACKS = 40;
const int SLICES = 40;

class Sphere
{
public:
	Sphere();
	~Sphere();

	void init(GLSLShader* shader);
	void update();
	void render(GLSLShader* shader);
	void close();

private:
	Mesh m_pSphereMesh;
	Vertex m_pVertices[(STACKS+1) * (SLICES+1)];
	int m_pNumVertices;
	GLushort m_pIndices[(STACKS * SLICES + SLICES)*6];
	int m_pNumIndices;
	GLenum m_pRendererMode;
	GLenum m_pPolygonMode;

	float m_pSphereRotation;
};

#endif
