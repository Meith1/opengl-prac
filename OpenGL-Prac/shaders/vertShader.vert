#version 330 core
 
layout(location = 0) in vec3 vVertex;	//object space vertex position
layout(location = 1) in vec3 vColor;	//per-vertex colour

//output from the vertex shader
out vec4 vSmoothColor;		//smooth colour to fragment shader

//uniform
uniform mat4 MVP;	//combined modelview projection matrix
uniform float theta;

void main()
{
	float angle = radians(theta);

	mat4 rz = mat4 (cos(angle), -sin(angle),  0.0,  0.0, 
                    sin(angle),  cos(angle),  0.0,  0.0,
                    0.0,  0.0,  1.0,  0.0,
                    0.0,  0.0,  0.0,  1.0);
	
	mat4 ry = mat4 (cos(angle), 0.0, -sin(angle), 0.0, 
                    0.0       , 1.0, 0.0        , 0.0,
                    sin(angle), 0.0, cos(angle) , 0.0,
                    0.0       , 0.0, 0.0        , 1.0);
					
	//assign the per-vertex colour to vSmoothColor varying
   vSmoothColor = vec4(vColor,1);

   //get the clip space position by multiplying the combined MVP matrix with the object space 
   //vertex position
   gl_Position = (rz*(ry*vec4(vVertex,1)));
}