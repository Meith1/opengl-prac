#ifndef __Mesh__
#define __Mesh__

#include "Vertex.h"
#include "GLSLShader.h"

#include <GL/glew.h>

class Mesh
{
public:
	Mesh();
	~Mesh();

	void init(Vertex vertices[], int numVertices, GLushort indices[], int numIndices, GLSLShader* shader);
	void update(float* rotation);
	void render(GLenum polygonMode, GLenum renderModem, int numIndices, GLSLShader* shader, float* rotation);
	void close();

private:
	GLuint m_pVaoID;
	GLuint m_pVboID;
	GLuint m_pVioID;
};

#endif
