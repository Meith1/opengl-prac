#ifndef __Suare__
#define __Square__

#include "Mesh.h"
#include "GLSLShader.h"

class Square
{
public:
	Square();
	~Square();

	void init(GLSLShader* shader);
	void update();
	void render(GLSLShader* shader);
	void close();

private:

	Mesh m_pSquareMesh;
	Vertex m_pVertices[4];
	int m_pNumVertices;
	GLushort m_pIndices[6];
	int m_pNumIndices;
	GLenum m_pRendererMode;
	GLenum m_pPolygonMode;

	float m_pSquareRoatation;
};

#endif
