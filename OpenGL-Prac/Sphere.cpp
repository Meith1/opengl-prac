#include "Sphere.h"

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include <vector>

Sphere::Sphere()
{
}

Sphere::~Sphere()
{
}

void Sphere::init(GLSLShader* shader)
{
	float radius = 0.5;

	//initialise all variables
	m_pNumVertices = (STACKS + 1) * (SLICES + 1);
	m_pNumIndices = (STACKS * SLICES + SLICES) * 6;
	m_pPolygonMode = GL_LINE;
	m_pRendererMode = GL_TRIANGLES;

	int k = 0;
	for (int i = 0; i <= STACKS; ++i)
	{
		float V = i / (float)STACKS;
		float phi = V * glm::pi <float>();

		// Loop Through Slices
		for (int j = 0; j <= SLICES; ++j)
		{
			float U = j / (float)SLICES;
			float theta = U * (glm::pi <float>() * 2);

			// Calc The Vertex Positions
			float x = cosf(theta) * sinf(phi);
			float y = cosf(phi);
			float z = sinf(theta) * sinf(phi);

			// Set Vertex Data
			m_pVertices[k].position.x = x * radius;
			m_pVertices[k].position.y = y * radius;
			m_pVertices[k].position.z = z * radius;
			k++;
		}
	}

	for (int i = 0; i < m_pNumVertices; i++)
	{
		m_pVertices[i].color.r = 0;
		m_pVertices[i].color.g = 0;
		m_pVertices[i].color.b = 0;
		m_pVertices[i].color.a = 255;
	}

	std::vector<GLushort> indices;
	for (int i = 0; i < SLICES * STACKS + SLICES; ++i)
	{
		indices.push_back(i);
		indices.push_back(i + SLICES + 1);
		indices.push_back(i + SLICES);

		indices.push_back(i + SLICES + 1);
		indices.push_back(i);
		indices.push_back(i + 1);
	}

	for (int i = 0; i < m_pNumIndices; i++)
		m_pIndices[i] = indices[i];
		
	m_pSphereRotation = 0.0f;

	m_pSphereMesh.init(m_pVertices, m_pNumVertices, m_pIndices, m_pNumIndices, shader);
}

void Sphere::update()
{
	m_pSphereMesh.update(&m_pSphereRotation);
}

void Sphere::render(GLSLShader* shader)
{
	m_pSphereMesh.render(m_pPolygonMode, m_pRendererMode, m_pNumIndices, shader, &m_pSphereRotation);
}

void Sphere::close()
{
	m_pSphereMesh.close();
}
